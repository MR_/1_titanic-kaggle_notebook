# Titanic - Machine Learning from Disaster 

This is a Notebook for the Kaggle competition, Titanic - Machine Learning from Disaster: 

https://www.kaggle.com/competitions/titanic/overview

## Goal

Predict if a passenger survived the sinking of the Titanic or not, building a predictive model that answers the question: “what sorts of people were more likely to survive?” using passenger data (ie name, age, gender, socio-economic class, etc). 

## Evaluation

The percentage of passengers correctly predicted. 

## Main Contents 

- Logistic Regression

## Kaggle's notebooks:

https://www.kaggle.com/code/mr0024/titanic-using-logistic-regression/notebook

## Requirements

The requirements file contains all the packages needed to work through this notebook
